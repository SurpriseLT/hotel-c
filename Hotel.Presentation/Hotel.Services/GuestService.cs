﻿using Hotel.Dal;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hotel.Dal.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace Hotel.Services
{
    public class GuestService : HotelDbContext
    {
        private readonly HotelDbContext _db;
        public GuestService()
        {
            _db = new HotelDbContext();
        }

        public List<Guest> GetGuests()
        {
            var duomenys = _db.Guests.Select(x => new Guest
            {
                ID = x.ID,
                Name = x.Name,
                LastName = x.LastName,
                Address = x.Address,
                Age = x.Age,
                City = x.City,
                Country = x.Country,
                ArrivalDate = x.ArrivalDate,
                DepartureDate = x.DepartureDate,
                Email = x.Email,
                Gender = x.Gender,
                HotelRoom = x.HotelRoom,
                PhoneNr = x.PhoneNr

            })
                //.Where()
                .ToList();
            return duomenys;
        }
        public void InsertOrUpdate()
        {
            //var guest = GetGuests().FirstOrDefault();
            //if (guest.ID == 0) //isertinti
            //    _db.Guests.Add(guest);
            //else // updatinti
            //{
            //    _db.Entry(guest).State = EntityState.Modified;
            //    _db.SaveChanges();
            //}
        }

       public void Delete(int id)
                {
            
            var guest = GetGuests().Where(x => x.ID == id).FirstOrDefault();
            var entry = _db.Entry(guest);
                    if (entry.State == EntityState.Detached)
                        _db.Guests.Attach(guest);
                    _db.Guests.Remove(guest);
                    _db.SaveChanges();
    

                }



  
    }
}
