﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hotel.Presentation.Guest_and_Agents;
using Hotel.Services;

namespace Hotel.Presentation.Main
{
    public partial class Main : Form
    {
        private readonly GuestService _guestService;
        private readonly RoomService _roomService;
        public Main()
        {
            InitializeComponent();
            _guestService = new GuestService();
            _roomService = new RoomService();
        }

        private void btnGuestList_Click(object sender, EventArgs e)
        {
        }

        private void pcboxGuest_DoubleClick_1(object sender, EventArgs e)
        {
            GuestList GuestList = new GuestList();
            GuestList.Show();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            foreach (var item in _roomService.HotelRooms().OrderBy(x => x.RoomTypeID))
            {
                cmbRoomType.Items.Add(item.RoomTypeID);

            }
        }
    }
}
