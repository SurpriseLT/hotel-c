drop schema if exists Hotel;
create schema if not exists Hotel;
use hotel;
create table tblGuest
(
	gID int auto_increment primary key,
    gName varchar(50),
    gLastName varchar(50),
    gAge int,
    gGender varchar(10),
    gPhoneNumber varchar (20),
    gAddress varchar (100),
    gCountry varchar (50),
    gCity varchar(50),
    gHotelRoom int,
    gArrivalDate varchar(20),
    gDepartureDate varchar(20),
    gEmail varchar(100)
);
create table tblStaff
(
	sStaffID int auto_increment primary key,
    sFirstName varchar(50),
    sLastName varchar(50),
    sAddress varchar(225),
    sCity varchar(50),
    sCountry varchar(50),
    sPhoneNumber varchar(20),
	sEmail varchar(100)
);
create table tblRoomStatus
(
	rsRoomStatusID int auto_increment primary key,
    rsRoomStatus bool,
    rsDescription varchar(225),
	rsActive bool
);
create table tblRoomTypes
(
	rtRoomTypeID int auto_increment primary key,
    rtRoomType varchar(50),
    rtDescription varchar(225),
    rtActive bool
);
create table tblReservationAgents
(
	raID int auto_increment primary key,
    raName varchar(50),
    raLastName varchar(50),
    raAddress varchar(100),
    raCountry varchar(50),
    raCity varchar(50),
    raPhoneNumber varchar(20),
    raGender varchar(10),
    raEmail varchar(100),
    raAge int
);
create table tblHotels
(
	hHotelID int auto_increment primary key,
    hHotelCode varchar(100),
    hName varchar(50),
    hMotto varchar(225),
    hAddress varchar(100),
    hCity varchar(50),
    hCountry varchar(50),
    hNumber varchar(20),
    hCompanyEmailAddress varchar(50)
);
create table tblBookingStatus
(
	bsBookingStatusID int auto_increment primary key,
    bsStatus int,
    bsDiscription varchar(225),
    bsActive bool
);
#Klaida
create table tblBookings
(
	bBookingID int auto_increment primary key,
    bHotelID int,
    bGuestID int,
    bBookingStatusID int,
    bReservationAgentID int,
    bDateFrom varchar(50),
	bDateTo varchar(50),
    bRoomCount int,
    foreign key (bHotelID) references tblHotels(hHotelID),
    foreign key (bGuestID) references tblGuest(gID),
    foreign key (bBookingStatusID) references tblBookingStatus(bsBookingStatusID),    
    foreign key (bReservationAgentID) references tblReservationAgents(raID)   
);
create table tblRoomsBooked
(
	rbRoomBookedID int auto_increment primary key,
    rbBookingId int,
    rbRoomID int,
    foreign key (rbBookingId) references tblBookings(bBookingID)
);

create table tblRooms
(
	rRoomID int auto_increment primary key,
    rHotelID int,
	rFloor int,
    rRoomTypeID int,
    rRoomNumber int,
    rDescription varchar(225),
    rRoomStatusID int,
    foreign key (rRoomStatusID) references tblRoomStatus(rsRoomStatusID),
    foreign key (rRoomTypeID) references tblRoomTypes(rtRoomTypeID),
    foreign key (rHotelID) references tblHotels(hHotelID)
);#*/
create table tblStaffRooms
(
	srStaffRoomID int auto_increment primary key,
    srRoomID int,
    srStaffID int,
    foreign key (srRoomID) references tblRooms(rRoomID),
    foreign key (srStaffID) references tblStaff(sStaffID)
);
create table tblUser
(
	uID int primary key auto_increment,
    uNickname varchar(50),
    uPassword varchar(50),
    uEmail varchar(100)
);

insert into tblGuest (gName, gLastName, gGender, gAge, gPhoneNumber, gAddress, gCountry, gCity, gHotelRoom, gArrivalDate, gDepartureDate, gEmail)
values 
('Marija','Kurnovka','Female',32,'+370656235','Verkiu s. 14','Lithuania','Vilnius',101,'2018-05-06','2018-05-16','Markius50@gmail.com'),
('Jonas','Paulauskas','Male',34,'+370651635','Verkiu s. 14','Lithuania','Vilnius',101,'2018-05-06','2018-05-16','Polskius@gmail.com'),
('Michale','Bernaton','Male',50,'+454256235','Bruklin s. 80','UnitedStates','NewYork',102,'2018-05-08','2018-05-18','WarMachine@gmail.com'),
('Jackson','Overmor','Male',22,'+9520256235','Merki s. 4','Germany','Berlin',103,'2018-05-30','2018-06-05','Grandbro@gmail.com');