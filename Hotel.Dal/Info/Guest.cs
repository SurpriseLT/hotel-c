﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Dal
{
    public class Guest
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string Address { get; set; }

        public string PhoneNr { get; set; }

        public int? Age { get; set; }

        public string Gender { get; set; }

        public string Country { get; set; }

        public string City { get; set; }

        public int? HotelRoom { get; set; }

        public string ArrivalDate { get; set; }

        public string DepartureDate { get; set; }

        public string Email{ get; set; }

    }
}
