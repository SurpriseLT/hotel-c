﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Dal.ViewModels
{
    public enum LoginStatus
    {
        wrongNick = 1,
        wrongPassword = 2,
        correct = 3
    }
}
