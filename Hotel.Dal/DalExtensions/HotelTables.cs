﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.Dal.DalExtensions
{
    public static class HotelTables
    {
        public static void MapUser(this ModelBuilder modelBuilder)
        {
            #region Guests
            //base.OnModelCreating(modelBuilder);
            var guestCfg = modelBuilder.Entity<Guest>();
            guestCfg.ToTable("tblGuest");
            guestCfg.HasKey(m => m.ID);
            guestCfg.Property(m => m.ID).HasColumnName("gID");
            guestCfg.Property(m => m.Name).HasColumnName("gName").HasMaxLength(50).IsUnicode(false);
            guestCfg.Property(m => m.LastName).HasColumnName("gLastName").HasMaxLength(50).IsUnicode(false);
            guestCfg.Property(m => m.Age).HasColumnName("gAge").HasMaxLength(11);
            guestCfg.Property(m => m.Gender).HasColumnName("gGender").HasMaxLength(10).IsUnicode(false);
            guestCfg.Property(m => m.PhoneNr).HasColumnName("gPhoneNumber").HasMaxLength(20).IsUnicode(false);
            guestCfg.Property(m => m.Address).HasColumnName("gAddress").HasMaxLength(100).IsUnicode(false);
            guestCfg.Property(m => m.Country).HasColumnName("gCountry").HasMaxLength(50).IsUnicode(false);
            guestCfg.Property(m => m.City).HasColumnName("gCity").HasMaxLength(50).IsUnicode(false);
            guestCfg.Property(m => m.HotelRoom).HasColumnName("gHotelRoom").HasMaxLength(11);
            guestCfg.Property(m => m.ArrivalDate).HasColumnName("gArrivalDate").HasMaxLength(20).IsUnicode(false);
            guestCfg.Property(m => m.DepartureDate).HasColumnName("gDepartureDate").HasMaxLength(20).IsUnicode(false);
            guestCfg.Property(m => m.Email).HasColumnName("gEmail").HasMaxLength(100).IsUnicode(false);
            #endregion

            #region User
            var userCfg = modelBuilder.Entity<User>();
            userCfg.ToTable("tblUser");
            userCfg.HasKey(m => m.ID);
            userCfg.Property(m => m.ID).HasColumnName("uID");
            userCfg.Property(m => m.Nickname).HasColumnName("uNickname");
            userCfg.Property(m => m.Password).HasColumnName("uPassword");
            userCfg.Property(m => m.Email).HasColumnName("uEmail");
            #endregion

            #region Rooms
            var roomcfg = modelBuilder.Entity<Room>();
            roomcfg.ToTable("tblRoom");
            roomcfg.HasKey(m => m.ID);
            roomcfg.Property(m => m.ID).HasColumnName("rID");
            roomcfg.Property(m => m.RoomNumber).HasColumnName("rRoomNumber");
            roomcfg.Property(m => m.RoomStatus).HasColumnName("rRoomStatus");
            roomcfg.Property(m => m.RoomTypeID).HasColumnName("rTypeID");
            #endregion
        }
    }
}
